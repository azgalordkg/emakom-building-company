from modeltranslation.translator import register, translator, TranslationOptions
from main.models import News, AboutCompany, Stock, Contacts, Testimonials, Buildings, BuildingFeatures, BuildingPlans, \
    StaticConfigs, BuildingPhotoReport


@register(Stock)
class StockTranslationOptions(TranslationOptions):
    fields = ('title', 'text', 'end_date')


@register(Contacts)
class ContactsTranslationOptions(TranslationOptions):
    fields = ('city', 'address', 'reference_point')


@register(News)
class NewsTranslationOptions(TranslationOptions):
    fields = ('title', 'video_file', 'text')


@register(Testimonials)
class TestimonialsTranslationOptions(TranslationOptions):
    fields = ('author', 'text')


@register(AboutCompany)
class AboutCompanyTranslationOptions(TranslationOptions):
    fields = ('title', 'text',)


@register(Buildings)
class BuildingsTranslationOptions(TranslationOptions):
    fields = ('type', 'title', 'description', 'features_text', 'address', 'end_date')


@register(BuildingFeatures)
class BuildingFeaturesTranslationOptions(TranslationOptions):
    fields = ('title',)


@register(BuildingPlans)
class BuildingPlansTranslationOptions(TranslationOptions):
    fields = ('description',)


@register(StaticConfigs)
class StaticConfigsTranslationOptions(TranslationOptions):
    fields = ('working_hours',)


@register(BuildingPhotoReport)
class BuildingPhotoReportTranslationOptions(TranslationOptions):
    fields = ('title',)
