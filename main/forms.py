from django import forms
from django.conf import settings
from nocaptcha_recaptcha import NoReCaptchaField

from nocaptcha_recaptcha.widgets import NoReCaptchaWidget
from main.models import Feedback


class InvisibleReCaptchaWidget(NoReCaptchaWidget):
    template = getattr(settings, "INVISIBLE_RECAPTCHA_WIDGET_TEMPLATE",
                       'nocaptcha_recaptcha/widget_invisible.html')


class FeedbackForm(forms.ModelForm):
    # capthca = NoReCaptchaField(
    #     gtag_attrs={
    #         'callback': 'onSubmit',  # name of JavaScript callback function
    #         'bind': 'send-feedback-button'  # submit button's ID in the form template
    #     },
    #     widget=InvisibleReCaptchaWidget
    # )

    class Meta:
        model = Feedback
        fields = '__all__'
