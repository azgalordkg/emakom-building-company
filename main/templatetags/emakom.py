import re

from django import template

register = template.Library()


@register.simple_tag
def get_lang_url(request, value):
    lang_url = request.path
    if not value == request.path.split('/')[1]:
        lang_url = request.path.replace(request.path.split('/')[1], value, 1)
    return lang_url


@register.simple_tag
def get_stock(building):
    return building.stock.all()


@register.filter
def is_big(forloop):
    old_forloop = forloop - 1
    if forloop % 3 == 0 and not forloop % 2 == 0:
        return True
    elif old_forloop % 3 == 0 and forloop % 2 == 0:
        return True
    else:
        return False


@register.filter
def is_mobile(request):
    MOBILE_AGENT_RE = re.compile(r".*(iphone|mobile|androidtouch)", re.IGNORECASE)
    if MOBILE_AGENT_RE.match(request.META['HTTP_USER_AGENT']):
        return True
    else:
        return False
