from main.models import Feedback

try:
    from django.utils.deprecation import MiddlewareMixin
except:
    MiddlewareMixin = object


class AdminMiddleWare(MiddlewareMixin):
    def process_request(self, request):
        if request.path.startswith('/admin/'):
            feedback = Feedback
            feedback_count = feedback.objects.filter(processed=False).count()
            if feedback_count > 0:
                feedback._meta.verbose_name_plural = "Заказы бесплатной консультации (%s)" % feedback_count
            else:
                feedback._meta.verbose_name_plural = "Заказы бесплатной консультации"
