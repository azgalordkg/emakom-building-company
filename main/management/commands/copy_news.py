from django.core.management import BaseCommand

from main.models import News


class Command(BaseCommand):
    def handle(self, *args, **options):
        # for i in range(25):
        #     news = News.objects.first()
        #     news.pk = None
        #     news.slug = news.slug + '-' + str(i)
        #     news.save()
        for i in News.objects.all():
            i.save()
