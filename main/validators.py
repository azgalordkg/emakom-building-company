from gettext import ngettext

from django.core.exceptions import ValidationError


class CodeValidator:
    """
    Validate whether the password is of a minimum length.
    """

    def __init__(self, min_length=8):
        self.min_length = min_length

    def validate(self, codes):
        if not ',' in codes:
            raise ValidationError(
                'Пожалуйста добавьте запятую после каждого кода оператора', )
        else:
            return

    def get_help_text(self):
        return "Не забывайте добаить запятую после каждого кода оператора"
