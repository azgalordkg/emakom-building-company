# Generated by Django 2.1.1 on 2018-11-26 11:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0020_auto_20181126_1033'),
    ]

    operations = [
        migrations.AddField(
            model_name='feedback',
            name='processed',
            field=models.BooleanField(default=False, verbose_name='Обработан'),
        ),
    ]
