# Generated by Django 2.1.3 on 2018-11-08 10:13

from django.db import migrations, models
import main.validators


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0004_contacts_reference_point'),
    ]

    operations = [
        migrations.CreateModel(
            name='Feedback',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('full_name', models.CharField(max_length=255, verbose_name='ФИО')),
                ('email', models.EmailField(max_length=254, verbose_name='Email')),
                ('question', models.TextField(max_length=500, verbose_name='Вопрос')),
            ],
            options={
                'verbose_name': 'Обратная связь',
                'verbose_name_plural': 'Обратная связь',
            },
        ),
        migrations.CreateModel(
            name='StaticConfigs',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('whatsapp', models.CharField(max_length=255, verbose_name='Номер для Whatsapp')),
                ('stock_background', models.ImageField(upload_to='', verbose_name='Задний фон для блока "Акции"')),
                ('email_to', models.EmailField(max_length=254, verbose_name='Email для отправки заявок')),
                ('jivosite_code', models.TextField(verbose_name='Код для Jivosite')),
            ],
            options={
                'verbose_name': 'Основные настройки',
                'verbose_name_plural': 'Основные настройки',
            },
        ),
        migrations.AlterField(
            model_name='contacts',
            name='phone_codes',
            field=models.CharField(help_text='Пишите коды через запятую', max_length=255, validators=[main.validators.CodeValidator], verbose_name='Коды операторов'),
        ),
        migrations.AlterField(
            model_name='contacts',
            name='working_hours',
            field=models.CharField(help_text='Для разделения вызодных и рабочих дней, используйте заятую', max_length=255, verbose_name='Время работы'),
        ),
        migrations.AlterField(
            model_name='contacts',
            name='working_hours_kg',
            field=models.CharField(help_text='Для разделения вызодных и рабочих дней, используйте заятую', max_length=255, null=True, verbose_name='Время работы'),
        ),
        migrations.AlterField(
            model_name='contacts',
            name='working_hours_ru',
            field=models.CharField(help_text='Для разделения вызодных и рабочих дней, используйте заятую', max_length=255, null=True, verbose_name='Время работы'),
        ),
    ]
