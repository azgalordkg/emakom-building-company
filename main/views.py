# Create your views here.
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView, DetailView, ListView, FormView, View

from main.forms import FeedbackForm
from main.models import Slider, AboutCompany, Certificates, Stock, Contacts, Testimonials, News, Buildings, \
    BuildingPlans
from main.utils import AjaxableResponseMixin


class IndexView(TemplateView):
    template_name = 'index.pug'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['slider'] = Slider.objects.all()
        context['about_company'] = AboutCompany.objects.first()
        context['certificates'] = Certificates.objects.all()
        context['stock'] = Stock.objects.all()
        context['contacts'] = Contacts.objects.all()
        context['testimonials'] = Testimonials.objects.all()
        context['news'] = News.objects.order_by('-created_at')[:3]
        context['buildings'] = Buildings.objects.all()[:6]

        return context


class WorksView(TemplateView):
    template_name = 'works.pug'

    def get_context_data(self, **kwargs):
        context = super(WorksView, self).get_context_data(**kwargs)
        context['slider'] = Slider.objects.all()
        context['about_company'] = AboutCompany.objects.first()
        context['certificates'] = Certificates.objects.all()
        context['stock'] = Stock.objects.all()
        context['contacts'] = Contacts.objects.all()
        context['testimonials'] = Testimonials.objects.all()
        context['news'] = News.objects.order_by('-created_at')
        return context


class ProjectListView(ListView):
    model = Buildings
    template_name = 'works.pug'
    context_object_name = 'buildings'


class ProjectView(DetailView):
    model = Buildings
    template_name = 'project.pug'
    context_object_name = 'building'

    def get_context_data(self, **kwargs):
        context = super(ProjectView, self).get_context_data(**kwargs)
        context['layouts'] = self.get_object().plans.filter(rooms=BuildingPlans.objects.order_by('rooms').first().rooms)
        context['buildings'] = self.model.objects.exclude(pk=self.get_object().pk)
        return context


class NewsView(ListView):
    model = News
    template_name = 'news.pug'
    context_object_name = 'news'
    paginate_by = 12

    def get_queryset(self):
        return super(NewsView, self).get_queryset().order_by('-created_at')


class NewsDetailView(DetailView):
    model = News
    template_name = 'news-element.pug'
    context_object_name = 'news'


class FeedbackCreateView(AjaxableResponseMixin, FormView):
    template_name = 'base.pug'
    form_class = FeedbackForm
    success_url = '/'


class FilterPlansByRoom(View):
    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super(FilterPlansByRoom, self).dispatch(request, *args, **kwargs)

    def post(self, request, building_pk, rooms):
        if request.is_ajax():
            layouts = BuildingPlans.objects.filter(building=int(building_pk), rooms=int(rooms))
            return render(request, 'include/layouts.pug', {'layouts': layouts})
