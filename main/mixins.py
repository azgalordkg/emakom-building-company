from django.contrib import messages


class SingleObjectAdminMixin(object):
    def save_model(self, request, obj, form, change):
        if self.model.objects.exists() and not change:
            old_pk = self.model.objects.first().pk
            obj.pk = old_pk
            messages.add_message(request, messages.WARNING, "Старый объект был успешно перезаписан")
        return super().save_model(request, obj, form, change)
