from django.contrib import admin
from django.contrib.auth.models import *
from django_2gis_maps.admin import DoubleGisAdmin
from modeltranslation.admin import TabbedTranslationAdmin, TranslationStackedInline

from main.models import Slider, AboutCompany, Certificates, Stock, Contacts, Testimonials, Feedback, StaticConfigs, \
    News, Buildings, BuildingImages, BuildingFeatures, BuildingViewsFormWindow, BuildingPhotoReport, \
    SocialLinks, BuildingPlanImages, BuildingPlans

# Register your models here.

admin.site.site_header = 'EMAKOM'
admin.site.unregister(Group)
admin.site.unregister(User)


class AboutCompanyAdmin(TabbedTranslationAdmin):
    def has_add_permission(self, request):
        if self.model.objects.count() >= 1:
            return False
        return True


class StockAdmin(TabbedTranslationAdmin):
    pass


class ContactsAdmin(DoubleGisAdmin, TabbedTranslationAdmin):
    multiple_markers = False


class TestimonialsAdmin(TabbedTranslationAdmin):
    pass


class NewsAdmin(TabbedTranslationAdmin):
    prepopulated_fields = ({'slug': ('title',)})
    list_display = ('title', 'created_at', 'updated_at')


class BuildingsSliderStackedInline(admin.StackedInline):
    model = BuildingImages
    extra = 1


class BuildingViewsFormWindowStackedInline(admin.StackedInline):
    model = BuildingViewsFormWindow
    extra = 1


class BuildingPhotoReportStackedInline(admin.StackedInline):
    model = BuildingPhotoReport
    extra = 1


class BuildingPlanImagesStackedInline(admin.StackedInline):
    model = BuildingPlanImages
    extra = 1


class BuildingPlansAdmin(TabbedTranslationAdmin):
    list_display = ('building', 'area', 'rooms')
    inlines = (BuildingPlanImagesStackedInline,)


class BuildingFeaturesStackedInline(admin.StackedInline):
    model = Buildings.features.through
    extra = 1


class BuildingAdmin(TabbedTranslationAdmin):
    inlines = (BuildingsSliderStackedInline, BuildingViewsFormWindowStackedInline,
               BuildingPhotoReportStackedInline, BuildingFeaturesStackedInline)
    exclude = ('features', 'features_ordering')

    # class Media:
    #     js = ('https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js', 'dist/admin/js/order.js',)
    #
    # def save_model(self, request, obj, form, change):
    #     super(BuildingAdmin, self).save_model(request, obj, form, change)
    # print(form.cleaned_data)
    # print(obj.features.all())


class BuildingFeaturesAdmin(TabbedTranslationAdmin):
    pass


class StaticConfigsAdmin(TabbedTranslationAdmin):
    pass


class FeedbackAdmin(admin.ModelAdmin):
    list_display = ('full_name', 'email', 'processed')


admin.site.register(Slider)
admin.site.register(SocialLinks)
admin.site.register(Feedback, FeedbackAdmin)
admin.site.register(News, NewsAdmin)
admin.site.register(StaticConfigs, StaticConfigsAdmin)
admin.site.register(AboutCompany, AboutCompanyAdmin)
admin.site.register(Certificates)
admin.site.register(Contacts, ContactsAdmin)
admin.site.register(Testimonials, TestimonialsAdmin)
admin.site.register(Stock, StockAdmin)
admin.site.register(Buildings, BuildingAdmin)
admin.site.register(BuildingPlans, BuildingPlansAdmin)
admin.site.register(BuildingFeatures, BuildingFeaturesAdmin)
