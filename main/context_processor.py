from django.urls import resolve

from main.forms import FeedbackForm
from main.models import Contacts, SocialLinks, StaticConfigs


def variables(request):
    context = {
        'header_contacts': Contacts.objects.filter(is_on_header=True).first(),
        'working_hours': StaticConfigs.objects.first(),
        'social_links': SocialLinks.objects.all(),
        'static_info': StaticConfigs.objects.first(),
        'contacts': Contacts.objects.all(),
        'feedback': FeedbackForm()
    }
    return context


def get_current_path_name(request):
    resolved = resolve(request.path_info)
    return {
        'path_name': resolved.url_name
    }
