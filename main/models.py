import datetime

import django
from ckeditor_uploader.fields import RichTextUploadingField
from django.contrib.auth.models import AbstractUser
from django.core.exceptions import ValidationError
from django.core.mail import EmailMessage
from django.db import models

# Create your models here.


# class User(AbstractUser):
#     pass
from django.db.models.signals import post_save
from django.template.loader import render_to_string
from django.urls import reverse
from django_2gis_maps.fields import AddressField, GeoLocationField
from django_2gis_maps.mixins import DoubleGisMixin

from main import validators


class Slider(models.Model):
    class Meta:
        verbose_name = 'Слайд'
        verbose_name_plural = 'Слайдер'

    image = models.ImageField(verbose_name='Картинка', upload_to='slider/images')

    def __str__(self):
        return str(self.image)


class SocialLinks(models.Model):
    class Meta:
        verbose_name = 'Социальная сеть'
        verbose_name_plural = 'Социальные сети'

    provider = models.CharField(verbose_name='Название соц сети', max_length=255)
    icon = models.FileField(verbose_name='Иконка соц сети', upload_to='social_links/icons')
    link = models.URLField(verbose_name='Ссылка')

    def __str__(self):
        return str(self.provider)


class AboutCompany(models.Model):
    class Meta:
        verbose_name = 'О компании'
        verbose_name_plural = 'О компании'

    title = models.CharField(verbose_name='Заголовок', max_length=255, null=True)
    text = RichTextUploadingField(verbose_name='Текст')

    def __str__(self):
        return "О компании"


class Buildings(models.Model):
    class Meta:
        verbose_name = 'Объект'
        verbose_name_plural = 'Объекты'

    image = models.ImageField(verbose_name='Превью', upload_to='buildings/images')
    type = models.CharField(verbose_name='Тип объека', help_text='Жилой комплекс, и т.д и т.п', max_length=255)
    title = models.CharField(verbose_name='Название объекта', max_length=255)
    address = models.CharField(verbose_name='Адрес', null=True, max_length=255)
    features_text = RichTextUploadingField(verbose_name='Описание для инфраструктуры', null=True, blank=True)
    features = models.ManyToManyField('BuildingFeatures', verbose_name='Инфраструктура')
    floor = models.PositiveSmallIntegerField(verbose_name='Количество этажей', default=1, null=True)
    floor_schema = models.ImageField(verbose_name='Планировка этажей', null=True)
    description = RichTextUploadingField(verbose_name='Описание', null=True)
    features_ordering = models.CharField(verbose_name='Порядок инфраструктуры', null=True, blank=True, max_length=255)
    end_date = models.CharField(verbose_name='Дата завершения', null=True, blank=True,
                                help_text='Если постройка уже завершена, оставьте поле пустым', max_length=255)

    def __str__(self):
        return str(self.title + ' --- ' + self.title)

    @property
    def get_plans(self):
        array = []
        for i in self.plans.all():
            if i.rooms not in array:
                array.append(i.rooms)
        return array

    def no_order_features(self):
        return BuildingFeatures.objects.filter(buildings=self)


class BuildingViewsFormWindow(models.Model):
    class Meta:
        verbose_name = 'Вид из окна'
        verbose_name_plural = 'Виды из окон'

    building = models.ForeignKey(Buildings, verbose_name='Объект', related_name='window_views',
                                 on_delete=models.CASCADE)
    image = models.ImageField(verbose_name='Картинка')


class BuildingPhotoReport(models.Model):
    class Meta:
        verbose_name = 'Фотоотчет'
        verbose_name_plural = 'Фотоотчеты'

    building = models.ForeignKey(Buildings, verbose_name='Объект', related_name='photo_report',
                                 on_delete=models.CASCADE)
    title = models.CharField(verbose_name='Заголовок', max_length=255, null=True, blank=True)
    image = models.ImageField(verbose_name='Картинка')


class BuildingFeatures(models.Model):
    class Meta:
        verbose_name = 'Инфраструктура'
        verbose_name_plural = 'Инфраструктура'

    title = models.CharField(verbose_name='Название', max_length=255)
    icon = models.ImageField(verbose_name='Иконка', upload_to='buildings/features')

    def __str__(self):
        return str(self.title)


class BuildingImages(models.Model):
    class Meta:
        verbose_name = 'Слайдер'
        verbose_name_plural = 'Слайдер'

    building = models.ForeignKey(Buildings, verbose_name='Объект', related_name='slider', on_delete=models.CASCADE)
    image = models.ImageField(verbose_name='Картинка', upload_to='buildings/slider')

    def __str__(self):
        return str(self.building)


class BuildingPlans(models.Model):
    class Meta:
        verbose_name = 'Планировка'
        verbose_name_plural = 'Планировка'

    building = models.ForeignKey(Buildings, verbose_name='Объект', related_name='plans', on_delete=models.CASCADE)
    rooms = models.PositiveSmallIntegerField(verbose_name='Количество комнат')
    area = models.FloatField(verbose_name='Квадратура')
    description = RichTextUploadingField(verbose_name='Описание')

    def __str__(self):
        return str(self.building)


class BuildingPlanImages(models.Model):
    class Meta:
        verbose_name = 'Слайдер для планироки'
        verbose_name_plural = 'Слайдер для планировки'

    plan = models.ForeignKey(BuildingPlans, verbose_name='Планировка', related_name='slider', on_delete=models.CASCADE)
    image = models.ImageField(verbose_name='Картинка', upload_to='buildings/plans/images')


class Contacts(DoubleGisMixin, models.Model):
    class Meta:
        verbose_name = 'Контакты'
        verbose_name_plural = 'Контакты'

    city = models.CharField(verbose_name='Город', max_length=255)
    address = AddressField(verbose_name='Адрес', max_length=255)
    reference_point = models.CharField(verbose_name='Ориентир', null=True, blank=True, max_length=255)
    geolocation = GeoLocationField(verbose_name='Координаты на карте')
    phone = models.CharField(verbose_name='Номер телефона', help_text='Номер телефона без кода, для этого города',
                             max_length=255)
    phone_codes = models.CharField(verbose_name='Коды операторов', validators=[validators.CodeValidator],
                                   max_length=255, help_text='Пишите коды через запятую')
    is_on_header = models.BooleanField(verbose_name='Отображать на шапке сайта')

    def __str__(self):
        return str(self.city)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        on_header = self.__class__.objects.filter(is_on_header=True)
        if on_header.exists() and self.is_on_header and self not in on_header:
            for i in on_header:
                i.is_on_header = False
                i.save()
        super(Contacts, self).save()

    @property
    def splited_codes(self):
        return self.phone_codes.split(',')


class Certificates(models.Model):
    class Meta:
        verbose_name = 'Сертификат'
        verbose_name_plural = 'Сертификаты'

    image = models.ImageField(verbose_name='Картинка', upload_to='certificates')

    def __str__(self):
        return str(self.image)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.__class__.objects.count() > 6:
            raise ValidationError("Вы можете загрузить только 6 сертификатов")
        return super(Certificates, self).save()


class Stock(models.Model):
    class Meta:
        verbose_name = 'Акция'
        verbose_name_plural = 'Акции'

    building = models.ForeignKey(Buildings, verbose_name='Объект', related_name='stock', on_delete=models.CASCADE,
                                 null=True, blank=True)
    title = models.CharField(verbose_name='Заголовок', max_length=255)
    image = models.ImageField(verbose_name='Картинка', upload_to='stock/images')
    text = RichTextUploadingField(verbose_name='Текст')
    created_at = models.DateTimeField(verbose_name='Дата создания', auto_now_add=True)
    end_date = models.CharField(verbose_name='Дата окончания', null=True, blank=True, max_length=255)

    def __str__(self):
        return str(self.title)


class Testimonials(models.Model):
    class Meta:
        verbose_name = 'Отзыв'
        verbose_name_plural = 'Отзывы'

    author = models.CharField(verbose_name='Автор', max_length=255)
    text = models.TextField(verbose_name='Текст', max_length=500)
    created_at = models.DateTimeField(verbose_name='Дата создания', auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name='Дата изменения', auto_now_add=True)

    def __str__(self):
        return str(self.author)


class News(models.Model):
    class Meta:
        verbose_name = 'Новость'
        verbose_name_plural = 'Новости'

    title = models.CharField(verbose_name='Заголовок', max_length=255)
    image = models.ImageField(verbose_name='Картинка', upload_to='news/images')
    video_file = models.FileField(verbose_name='Видео файл', upload_to='news/videos', null=True, blank=True)
    text = RichTextUploadingField(verbose_name='Текст')
    slug = models.SlugField(verbose_name='Ярлык', help_text='Ярлыки не должны повторяться!', unique=True, null=True)
    created_at = models.DateTimeField(verbose_name='Дата создания', null=True, default=django.utils.timezone.now)
    updated_at = models.DateTimeField(verbose_name='Дата изменения', auto_now=True, null=True)

    def __str__(self):
        return str(self.title)


class StaticConfigs(models.Model):
    class Meta:
        verbose_name = 'Основные настройки'
        verbose_name_plural = 'Основные настройки'

    working_hours = models.CharField(verbose_name='Время работы', max_length=255,
                                     help_text='Для разделения выходных и рабочих дней, используйте заятую', null=True)
    news_video = models.FileField(verbose_name='Видео на странице новостей', null=True, blank=True)
    whatsapp = models.CharField(verbose_name='Номер для Whatsapp', max_length=255)
    stock_background = models.ImageField(verbose_name='Задний фон для блока "Акции"')
    email_to = models.EmailField(verbose_name='Email для отправки заявок')
    jivosite_code = models.TextField(verbose_name='Код для Jivosite', null=True, blank=True)

    def __str__(self):
        return str(self._meta.verbose_name_plural)

    @property
    def splited_working_hours(self):
        return self.working_hours.split(',')


class Feedback(models.Model):
    class Meta:
        verbose_name = 'Заказ бесплатной консультации'
        verbose_name_plural = 'Заказы бесплатной консультации'

    full_name = models.CharField(verbose_name='ФИО', max_length=255)
    email = models.CharField(verbose_name='Email или телефон', max_length=255)
    question = models.TextField(verbose_name='Вопрос', max_length=500)
    processed = models.BooleanField(verbose_name='Обработан', default=False)

    def __str__(self):
        return str(self.full_name)

    def send_notify(self, context):
        template = 'email/feedback.html'
        mail_subject = 'Новая заявка'
        to_email = StaticConfigs.objects.first().email_to
        email = EmailMessage(
            mail_subject, render_to_string(template, context), to=[to_email]
        )
        email.send()


def send_notification(sender, instance, created, **kwargs):
    if created:
        context = {
            'full_name': instance.full_name,
            'email': instance.email,
            'question': instance.question
        }
        return instance.send_notify(context)
    return False


def pre_save_news(sender, instance, created, **kwargs):
    if not instance.created_at:
        instance.created_at = datetime.datetime.now()
        instance.save()


post_save.connect(send_notification, sender=Feedback)
post_save.connect(pre_save_news, sender=News)
