import datetime
import os
from fabric.context_managers import cd, prefix
from fabric.operations import sudo, run, local
from fabric.state import env

PROJECT_NAME = 'emakom'
PROJECT_ROOT = '/var/www/html/emakom'
VENV_DIR = '/var/www/html/emakom_env'


def migrate():
    local('./manage.py makemigrations')
    local('./manage.py migrate')


def commit():
    # local('pip freeze > requirements/base.txt')
    local('git add .')
    local('git commit -m "' + str(datetime.datetime.today().date()) + '"')
    local('git push origin master')


def deploy():
    migrate()
    commit()
    env.host_string = os.environ.get('SERVER_IP', '78.24.216.99')
    env.user = os.environ.get('SERVER_USER', 'root')
    env.password = os.environ.get('SERVER_PASSWORD', '18j2b332QajD')
    with cd(PROJECT_ROOT):
        sudo('git stash')
        sudo('git pull origin master')
        with prefix('source ' + VENV_DIR + '/bin/activate'):
            run('pip install -r requirements/base.txt')
            run('./manage.py collectstatic --noinput')
            run('./manage.py migrate')
            run('./manage.py makemessages -l kg')
            run('./manage.py makemessages -l kg -e pug')
            run('./manage.py compilemessages')
            sudo('service emakom restart')
            sudo('service nginx restart')