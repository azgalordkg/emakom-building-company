"""djps URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from djps import settings
from main.views import IndexView, WorksView, ProjectView, NewsView, NewsDetailView, FeedbackCreateView, ProjectListView, \
    FilterPlansByRoom

urlpatterns = [
    path('admin/', admin.site.urls),
    path('jet/', include('jet.urls', 'jet')),
    path('ckeditor/', include('ckeditor_uploader.urls')),
    path('_nested_admin/', include('nested_admin.urls')),

]
if 'rosetta' in settings.INSTALLED_APPS:
    urlpatterns += [
        url(r'^rosetta/', include('rosetta.urls'))

    ]
urlpatterns += i18n_patterns(
    path('', IndexView.as_view(), name='main'),
    # path('works/', WorksView.as_view(), name='works'),
    path('projects/', ProjectListView.as_view(), name='projects'),
    path('projects/<int:pk>', ProjectView.as_view(), name='project'),
    path('news/', NewsView.as_view(), name='news'),
    path('news/<slug>', NewsDetailView.as_view(), name='news_detail'),
    path('feedback/', FeedbackCreateView.as_view(), name='feedback'),
    path('filter_layouts_by_room/<int:building_pk>/<int:rooms>', FilterPlansByRoom.as_view(), name='filter_room')
)
# STATIC
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
